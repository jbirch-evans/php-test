@extends('layouts.master')

@section('content')

	<div id="pokedex-header">
		<div class="row">
			<h1 class="m-auto">Pokedex</h1>
		</div>
		<div class="row">
			<input type="text" id="pokemon-search" class="form-control m-auto">
		</div>
	</div>


	<div id="pokemon-list">
		<table id="pokemon-table" class="display">
		    <thead>
		        <tr>
		            <th>Pokemon Name</th>
		        </tr>
		    </thead>
		    <tbody>
		    	@foreach ($pokemonList as $pokemon)
					<tr href="/pokemon/{{ $pokemon->name }}"><td>{{ ucfirst($pokemon->name) }}</td></tr>
				@endforeach
		    </tbody>
		</table>
	</div>

	<div id="nav-buttons">
		<button id="previous-button" class="page-buttons"><</button>
		<button id="next-button" class="page-buttons">></button>
	</div>
	
@endsection

@section('pageScripts')
	<script>
		$('#pokemon-table tbody tr').click(function() {
			window.location.href = $(this).attr('href');
		});

		let pokemonTable = $('#pokemon-table').DataTable({
			paging: true,
			dom: 't',	
			ordering: false,
		});

		// hide the table head
		$('#pokemon-table thead').hide();

		// Search the table
		$('#pokemon-search').keyup(function(e) {
			pokemonTable.search($(this).val()).draw();
		});

		// Setup click event listeners for your buttons
		$('#next-button').on('click', function() {
			pokemonTable.page( 'next' ).draw( 'page' );
		});

		$('#previous-button').on('click', function() {
			pokemonTable.page( 'previous' ).draw( 'page' );
		});
		
	</script>
@endsection