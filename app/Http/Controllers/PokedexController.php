<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

use GuzzleHttp\Client;

class PokedexController extends Controller
{

	// function to request data from API using guzzle
	public function getPokemonApiData($cacheName, $url) {

		// Check if the data is cached, TRUE - Return cached data, FALSE - Make request, chache and return data
		if(Cache::has($cacheName)) {
			return Cache::get($cacheName);
		} else {
			// http request to get data from API
			$client = new Client([
				'base_uri'	=> 'https://pokeapi.co/',
				'timeout'	=> 5.0
			]);
			$response = $client->request('GET', $url);
			
			$statusCode = $response->getStatusCode();
			$reason = $response->getReasonPhrase();
			$data = json_decode((string) $response->getBody());

			// Check if the response was successful
			if($statusCode == 200) {
				// Check if the data contained the property results
				if(property_exists($data, 'results')) {
					$data = $data->results;
				}
				// Cache data
				Cache::put($cacheName, $data, now()->addDay());
				// Return data
				return $data;
			} else {
				return 'Error ' . $statusCode . ': ' . $reason;
			}
		}
	}


	// Some of the pokemon don't have image as the are a variant of a pokemon, function to check, if FALSE use default
	public function checkIfPokemonHasImage($pokemon) {
		$pokemonImage = $pokemon->sprites->front_default;

		if($pokemonImage == null) {
			$defaultVariation;
			// Get the pokemons species
			$species = $pokemon->species->name;

			// Get the species data
			$speciesData = $this->getPokemonApiData($species . '-species-data', '/api/v2/pokemon-species/' . $species);

			$foundDefault = false;
			$counter = 0;

			// dd($speciesData->varieties[0]->is_default);

			// Loop throught to find the default variation
			while (!$foundDefault) {
				if($speciesData->varieties[$counter]->is_default) {
					$defaultVariation = $speciesData->varieties[$counter]->pokemon->name;
					$foundDefault= true;
				}
				$counter++;
			}

			// Get the default pokemons data
			$pokemonData = $this->getPokemonApiData($defaultVariation . '-data', '/api/v2/pokemon/' . $defaultVariation);

			// Return image path
			return $pokemonData->sprites->front_default;
		} else {
			return $pokemonImage;
		}
	}


    // Return the pokedex view with a list of pokmon
    public function pokedex() {
    	$pokemonList = $this->getPokemonApiData('pokemonList', '/api/v2/pokemon');

    	return view('pokedex', compact('pokemonList'));
    }


    //Returns the data for a specific pokemon and returns a view to display the data
    public function pokemonData($name) {
    	$abiltitiesData = array();
    	$pokemonImagePath;

    	$pokemonData = $this->getPokemonApiData($name . '-data', '/api/v2/pokemon/' . $name);

    	// Loop through abilities and get the data
    	foreach ($pokemonData->abilities as $abilityData) {
    		$abilityName = $abilityData->ability->name;

    		// Get the abilties details via API
    		$abilityDetails = $this->getPokemonApiData($abilityName . '-data', '/api/v2/ability/' . $abilityName);

    		// Push the required data to an array
    		$abilitiesArray[$abilityDetails->name] = $abilityDetails->effect_entries[0]->effect;
    	}

    	// Check if the pokemon has an image path cached
    	if(Cache::has($name . '-image-path')) {
			$pokemonImagePath = Cache::get($name . '-image-path');
		} else {
			// check for image
			$imgPath = $this->checkIfPokemonHasImage($pokemonData);
			// Save path to cache
			Cache::put($name . '-image-path', $imgPath, now()->addDay());
			$pokemonImagePath = $imgPath;
		}

    	return view('pokemon_data', compact('pokemonData', 'abilitiesArray', 'pokemonImagePath'));
    }
}
