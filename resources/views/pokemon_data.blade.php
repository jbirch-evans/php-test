@extends('layouts.master')

@section('content')
	<div id="pokedex-header">
		<div class="row">
			<h1 class="m-auto">Pokedex</h1>
		</div>
	</div>

	<div id="pokemon-data">
		<div id="pokemon-name">
			<h2>{{ ucfirst($pokemonData->name) }}</h2>
		</div>
		<div id="pokemon-image" class="m-auto">
			<img src="{{ $pokemonImagePath }}" style="width: 200px;">
		</div>
		<div id="pokemon-basic-info">
			<div class="row">
				<div class="col info-title">Species:</div>
				<div class="col info-data">{{ $pokemonData->species->name }}</div>
			</div>
			<div class="row">
				<div class="col info-title">Height:</div>
				<div class="col info-data">{{ $pokemonData->height }}</div>
			</div>
			<div class="row">
				<div class="col info-title">Weight:</div>
				<div class="col info-data">{{ $pokemonData->weight }}</div>
			</div>
		</div>
		<div id="pokemon-abilities">
			<div>
				<h3 class="m-auto">Abilities</h3>
			</div>
			<div id="abilty-container">
				@foreach ($abilitiesArray as $abilityName => $abilityDescription)
					<div class="abilityName">
						<h4>{{ $abilityName }}</h4>
					</div>
					<div class="ability-description">
						<p>{{ $abilityDescription }}</p>
					</div>
				@endforeach
			</div>
		</div>
	</div>
	<div id="home-button-container">
		<a href="/"><i class="fas fa-home fa-3x"></i></a>
	</div>
@endsection